const mongoose = require("mongoose");
const Schema   = mongoose.Schema;

const authSchema = new Schema({

  username: {type:String, unique:true},
  email: {type:String, unique:true},
  password: String,
  fullname:String,
  isAdmin:Boolean,

}, {
  timestamps: true
});

const Auth = mongoose.model("Auth", authSchema);

module.exports = Auth;