const express = require('express');
const router = express.Router();
const User = require("../models/Auth");
const passport = require("passport");
const ensureLogin = require("connect-ensure-login");


//Mainpage

router.get('/home',(req,res,next)=>{
  
  res.render("homepage");
  
})


module.exports = router;