const express = require('express');
const router = express.Router();
const User = require("../models/Auth");
const passport = require("passport");
const session = require("express-session");
const ensureLogin = require("connect-ensure-login");

/* GET home page */
router.get('/', (req, res, next) => {
  res.render('homepage');
});


// BCrypt to encrypt passwords
const bcrypt = require("bcryptjs");
const bcryptSalt = 10;

router.get('/signup', (req, res, next) => {

  res.render('admin/signup');

})

router.post("/signup", (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;
  const email = req.body.email;
  const fullname = req.body.fullname;
  const isAdmin = req.body.isAdmin;

  if (username === '' || password === '' || email ==='') {
    res.redirect('/signup')
    return
  } else {


    const salt = bcrypt.genSaltSync(bcryptSalt);
    const hashPass = bcrypt.hashSync(password, salt);

    User.findOne({
        "username": username
      })
      .then(user => {
        if (user !== null) {
          res.redirect("/signup");
          return;
        }
        //--------



        User.create({
          username,
          email,
          password: hashPass,
          fullname,
          isAdmin,
        }).then((user) => {



          res.redirect("/")



        }).catch(error => {
          console.log(error);
        })
      }).catch(err => {
        console.log(err)
      });
  }
});





//login Passport Style

router.get("/login",(req,res,next)=>{
  res.render('login');
})


router.post("/login", passport.authenticate("local", {
  successRedirect: "/userDash",
  failureRedirect: "/login",
  failureFlash: true,
  passReqToCallback: true
}));


//Logout PassPort Style
router.get('/logout', (req, res, next) => {
  //req.flash('success', "You Have Been Logged Out");
  
  req.logout();
  res.redirect("/login");
})



//Panels

router.get('/adminpanel', ensureLogin.ensureLoggedIn("/home"), (req, res, next) => {
  User.findById(req.user._id)
    .then(theUser=> {
  
    if(theUser.isAdmin){
  res.render('admin/adminPanel');
    }else{
      res.redirect("/home")
    }
})

})


router.get("/userDash",ensureLogin.ensureLoggedIn('login'),(req,res,next)=>{
    User.findById(req.user._id)
    .then(theUser=> {
  
    if(theUser.isAdmin){
  
    res.render("admin/adminPanel");

    }
    else{
  
     res.render('user/userDash');
  
    }
  })
  })


// router.post('/deleteaccount', (req, res, next) => {
  //   User.findByIdAndDelete(req.user._id).then(() => {
    
    //     res.redirect('/trading')
    
//   })
// })






module.exports = router;
